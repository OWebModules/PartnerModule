﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Runtime.InteropServices;
using OntologyClasses.Interfaces;

namespace PartnerModule
{
    public class clsLocalConfig : ILocalConfig
{
    private const string cstrID_Ontology = "0c5596776b634fc3a9e63858758c8e57";
    private XMLImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

    public clsOntologyItem OItem_adress_zusatz { get; set; }
public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
public clsOntologyItem OItem_attribute_etin { get; set; }
public clsOntologyItem OItem_attribute_geburtsdatum { get; set; }
public clsOntologyItem OItem_attribute_identifkationsnummer__idnr_ { get; set; }
public clsOntologyItem OItem_attribute_nach_vereinbarung { get; set; }
public clsOntologyItem OItem_attribute_nachname { get; set; }
public clsOntologyItem OItem_attribute_postfach { get; set; }
public clsOntologyItem OItem_attribute_sozialversicherungsnummer { get; set; }
public clsOntologyItem OItem_attribute_straße { get; set; }
public clsOntologyItem OItem_attribute_timestamp__from_ { get; set; }
public clsOntologyItem OItem_attribute_timestamp__to_ { get; set; }
public clsOntologyItem OItem_attribute_todesdatum { get; set; }
public clsOntologyItem OItem_attribute_vorname { get; set; }
public clsOntologyItem OItem_attribute_zusatz { get; set; }
public clsOntologyItem OItem_class_adress_zusatz { get; set; }
public clsOntologyItem OItem_class_email_type { get; set; }
public clsOntologyItem OItem_class_telefonnummern_typ { get; set; }
public clsOntologyItem OItem_class_url_type { get; set; }
public clsOntologyItem OItem_class_zusatz_typ { get; set; }
public clsOntologyItem OItem_object_partner_management { get; set; }
public clsOntologyItem OItem_object_standard_addresszusatz { get; set; }
public clsOntologyItem OItem_object_standard_emailtype { get; set; }
public clsOntologyItem OItem_object_standard_teltype { get; set; }
public clsOntologyItem OItem_object_standard_urltype { get; set; }
public clsOntologyItem OItem_relationtype_belonging { get; set; }
public clsOntologyItem OItem_relationtype_belonging_photo { get; set; }
public clsOntologyItem OItem_relationtype_belongsto { get; set; }
public clsOntologyItem OItem_relationtype_contact { get; set; }
public clsOntologyItem OItem_relationtype_contains { get; set; }
public clsOntologyItem OItem_relationtype_fax { get; set; }
public clsOntologyItem OItem_relationtype_has { get; set; }
public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
public clsOntologyItem OItem_relationtype_isinstate { get; set; }
public clsOntologyItem OItem_relationtype_located_in { get; set; }
public clsOntologyItem OItem_relationtype_needs { get; set; }
public clsOntologyItem OItem_relationtype_offered_by { get; set; }
public clsOntologyItem OItem_relationtype_sitz { get; set; }
public clsOntologyItem OItem_relationtype_tel { get; set; }
        public clsOntologyItem OItem_relationtype_geboren_in { get; set; }
        public clsOntologyItem OItem_type_address { get; set; }
public clsOntologyItem OItem_type_availabilities { get; set; }
public clsOntologyItem OItem_type_availability_data { get; set; }
public clsOntologyItem OItem_type_email_address { get; set; }
public clsOntologyItem OItem_type_etin { get; set; }
public clsOntologyItem OItem_type_familienstand { get; set; }
public clsOntologyItem OItem_type_geschlecht { get; set; }
public clsOntologyItem OItem_type_identifkationsnummer__idnr_ { get; set; }
public clsOntologyItem OItem_type_images__graphic_ { get; set; }
public clsOntologyItem OItem_type_kommunikationsangaben { get; set; }
public clsOntologyItem OItem_type_land { get; set; }
public clsOntologyItem OItem_type_module { get; set; }
public clsOntologyItem OItem_type_nat_rliche_person { get; set; }
public clsOntologyItem OItem_type_ort { get; set; }
public clsOntologyItem OItem_type_partner { get; set; }
public clsOntologyItem OItem_type_partner_module { get; set; }
public clsOntologyItem OItem_type_postleitzahl { get; set; }
public clsOntologyItem OItem_type_sozialversicherungsnummer { get; set; }
public clsOntologyItem OItem_type_steuernummer { get; set; }
public clsOntologyItem OItem_type_telefonnummer { get; set; }
public clsOntologyItem OItem_type_url { get; set; }
public clsOntologyItem OItem_type_web_service { get; set; }
public clsOntologyItem OItem_type_weekdays { get; set; }



private void get_Data_DevelopmentConfig()
{
    var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

    var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
    {
        if (objDBLevel_Config1.ObjectRels.Any())
        {

            objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
            {
                ID_Object = oi.ID_Other,
                ID_RelationType = Globals.RelationType_belongingAttribute.GUID
            }).ToList();

            objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
            {
                ID_Object = oi.ID_Other,
                ID_RelationType = Globals.RelationType_belongingClass.GUID
            }));
            objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
            {
                ID_Object = oi.ID_Other,
                ID_RelationType = Globals.RelationType_belongingObject.GUID
            }));
            objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
            {
                ID_Object = oi.ID_Other,
                ID_RelationType = Globals.RelationType_belongingRelationType.GUID
            }));

            objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (!objDBLevel_Config2.ObjectRels.Any())
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }
        }
        else
        {
            throw new Exception("Config-Error");
        }

    }

}

public clsLocalConfig()
{
    Globals = new Globals();
    set_DBConnection();
    get_Config();
}

public clsLocalConfig(Globals Globals)
{
    this.Globals = Globals;
    set_DBConnection();
    get_Config();
}

private void set_DBConnection()
{
    objDBLevel_Config1 = new OntologyModDBConnector(Globals);
    objDBLevel_Config2 = new OntologyModDBConnector(Globals);
    objImport = new XMLImportWorker(Globals);
}

private void get_Config()
{
    try
    {
        get_Data_DevelopmentConfig();
        get_Config_AttributeTypes();
        get_Config_RelationTypes();
        get_Config_Classes();
        get_Config_Objects();
    }
    catch (Exception ex)
    {
        var objAssembly = Assembly.GetExecutingAssembly();
        AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
        var strTitle = "Unbekannt";
        if (objCustomAttributes.Length == 1)
        {
            strTitle = objCustomAttributes.First().Title;
        }
        var objOItem_Result = objImport.ImportTemplates(objAssembly);
        if (objOItem_Result.GUID != Globals.LState_Error.GUID)
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        else
        {
            throw new Exception("Config not importable");
        }
        
    }
}

private void get_Config_AttributeTypes()
{
    var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

    if (objOList_attribute_dbpostfix.Any())
    {
        OItem_attribute_dbpostfix = new clsOntologyItem()
        {
            GUID = objOList_attribute_dbpostfix.First().ID_Other,
            Name = objOList_attribute_dbpostfix.First().Name_Other,
            GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_etin = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "attribute_etin".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                   select objRef).ToList();

    if (objOList_attribute_etin.Any())
    {
        OItem_attribute_etin = new clsOntologyItem()
        {
            GUID = objOList_attribute_etin.First().ID_Other,
            Name = objOList_attribute_etin.First().Name_Other,
            GUID_Parent = objOList_attribute_etin.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_geburtsdatum = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attribute_geburtsdatum".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

    if (objOList_attribute_geburtsdatum.Any())
    {
        OItem_attribute_geburtsdatum = new clsOntologyItem()
        {
            GUID = objOList_attribute_geburtsdatum.First().ID_Other,
            Name = objOList_attribute_geburtsdatum.First().Name_Other,
            GUID_Parent = objOList_attribute_geburtsdatum.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_identifkationsnummer__idnr_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "attribute_identifkationsnummer__idnr_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                          select objRef).ToList();

    if (objOList_attribute_identifkationsnummer__idnr_.Any())
    {
        OItem_attribute_identifkationsnummer__idnr_ = new clsOntologyItem()
        {
            GUID = objOList_attribute_identifkationsnummer__idnr_.First().ID_Other,
            Name = objOList_attribute_identifkationsnummer__idnr_.First().Name_Other,
            GUID_Parent = objOList_attribute_identifkationsnummer__idnr_.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_nach_vereinbarung = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_nach_vereinbarung".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

    if (objOList_attribute_nach_vereinbarung.Any())
    {
        OItem_attribute_nach_vereinbarung = new clsOntologyItem()
        {
            GUID = objOList_attribute_nach_vereinbarung.First().ID_Other,
            Name = objOList_attribute_nach_vereinbarung.First().Name_Other,
            GUID_Parent = objOList_attribute_nach_vereinbarung.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_nachname = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "attribute_nachname".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                       select objRef).ToList();

    if (objOList_attribute_nachname.Any())
    {
        OItem_attribute_nachname = new clsOntologyItem()
        {
            GUID = objOList_attribute_nachname.First().ID_Other,
            Name = objOList_attribute_nachname.First().Name_Other,
            GUID_Parent = objOList_attribute_nachname.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_postfach = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "attribute_postfach".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                       select objRef).ToList();

    if (objOList_attribute_postfach.Any())
    {
        OItem_attribute_postfach = new clsOntologyItem()
        {
            GUID = objOList_attribute_postfach.First().ID_Other,
            Name = objOList_attribute_postfach.First().Name_Other,
            GUID_Parent = objOList_attribute_postfach.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_sozialversicherungsnummer = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "attribute_sozialversicherungsnummer".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                        select objRef).ToList();

    if (objOList_attribute_sozialversicherungsnummer.Any())
    {
        OItem_attribute_sozialversicherungsnummer = new clsOntologyItem()
        {
            GUID = objOList_attribute_sozialversicherungsnummer.First().ID_Other,
            Name = objOList_attribute_sozialversicherungsnummer.First().Name_Other,
            GUID_Parent = objOList_attribute_sozialversicherungsnummer.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_straße = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "attribute_straße".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                     select objRef).ToList();

    if (objOList_attribute_straße.Any())
    {
        OItem_attribute_straße = new clsOntologyItem()
        {
            GUID = objOList_attribute_straße.First().ID_Other,
            Name = objOList_attribute_straße.First().Name_Other,
            GUID_Parent = objOList_attribute_straße.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_timestamp__from_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attribute_timestamp__from_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

    if (objOList_attribute_timestamp__from_.Any())
    {
        OItem_attribute_timestamp__from_ = new clsOntologyItem()
        {
            GUID = objOList_attribute_timestamp__from_.First().ID_Other,
            Name = objOList_attribute_timestamp__from_.First().Name_Other,
            GUID_Parent = objOList_attribute_timestamp__from_.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_timestamp__to_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "attribute_timestamp__to_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                             select objRef).ToList();

    if (objOList_attribute_timestamp__to_.Any())
    {
        OItem_attribute_timestamp__to_ = new clsOntologyItem()
        {
            GUID = objOList_attribute_timestamp__to_.First().ID_Other,
            Name = objOList_attribute_timestamp__to_.First().Name_Other,
            GUID_Parent = objOList_attribute_timestamp__to_.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_todesdatum = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "attribute_todesdatum".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                         select objRef).ToList();

    if (objOList_attribute_todesdatum.Any())
    {
        OItem_attribute_todesdatum = new clsOntologyItem()
        {
            GUID = objOList_attribute_todesdatum.First().ID_Other,
            Name = objOList_attribute_todesdatum.First().Name_Other,
            GUID_Parent = objOList_attribute_todesdatum.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_vorname = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "attribute_vorname".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                      select objRef).ToList();

    if (objOList_attribute_vorname.Any())
    {
        OItem_attribute_vorname = new clsOntologyItem()
        {
            GUID = objOList_attribute_vorname.First().ID_Other,
            Name = objOList_attribute_vorname.First().Name_Other,
            GUID_Parent = objOList_attribute_vorname.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_attribute_zusatz = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "attribute_zusatz".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                     select objRef).ToList();

    if (objOList_attribute_zusatz.Any())
    {
        OItem_attribute_zusatz = new clsOntologyItem()
        {
            GUID = objOList_attribute_zusatz.First().ID_Other,
            Name = objOList_attribute_zusatz.First().Name_Other,
            GUID_Parent = objOList_attribute_zusatz.First().ID_Parent_Other,
            Type = Globals.Type_AttributeType
        };
    }
    else
    {
        throw new Exception("config err");
    }


}

private void get_Config_RelationTypes()
{
            var objOList_relationtype_geboren_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_geboren_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_geboren_in.Any())
            {
                OItem_relationtype_geboren_in = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_geboren_in.First().ID_Other,
                    Name = objOList_relationtype_geboren_in.First().Name_Other,
                    GUID_Parent = objOList_relationtype_geboren_in.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_belonging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

    if (objOList_relationtype_belonging.Any())
    {
        OItem_relationtype_belonging = new clsOntologyItem()
        {
            GUID = objOList_relationtype_belonging.First().ID_Other,
            Name = objOList_relationtype_belonging.First().Name_Other,
            GUID_Parent = objOList_relationtype_belonging.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_belonging_photo = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_belonging_photo".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

    if (objOList_relationtype_belonging_photo.Any())
    {
        OItem_relationtype_belonging_photo = new clsOntologyItem()
        {
            GUID = objOList_relationtype_belonging_photo.First().ID_Other,
            Name = objOList_relationtype_belonging_photo.First().Name_Other,
            GUID_Parent = objOList_relationtype_belonging_photo.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

    if (objOList_relationtype_belongsto.Any())
    {
        OItem_relationtype_belongsto = new clsOntologyItem()
        {
            GUID = objOList_relationtype_belongsto.First().ID_Other,
            Name = objOList_relationtype_belongsto.First().Name_Other,
            GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_contact = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "relationtype_contact".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                         select objRef).ToList();

    if (objOList_relationtype_contact.Any())
    {
        OItem_relationtype_contact = new clsOntologyItem()
        {
            GUID = objOList_relationtype_contact.First().ID_Other,
            Name = objOList_relationtype_contact.First().Name_Other,
            GUID_Parent = objOList_relationtype_contact.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                          select objRef).ToList();

    if (objOList_relationtype_contains.Any())
    {
        OItem_relationtype_contains = new clsOntologyItem()
        {
            GUID = objOList_relationtype_contains.First().ID_Other,
            Name = objOList_relationtype_contains.First().Name_Other,
            GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_fax = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "relationtype_fax".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                     select objRef).ToList();

    if (objOList_relationtype_fax.Any())
    {
        OItem_relationtype_fax = new clsOntologyItem()
        {
            GUID = objOList_relationtype_fax.First().ID_Other,
            Name = objOList_relationtype_fax.First().Name_Other,
            GUID_Parent = objOList_relationtype_fax.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_has = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "relationtype_has".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                     select objRef).ToList();

    if (objOList_relationtype_has.Any())
    {
        OItem_relationtype_has = new clsOntologyItem()
        {
            GUID = objOList_relationtype_has.First().ID_Other,
            Name = objOList_relationtype_has.First().Name_Other,
            GUID_Parent = objOList_relationtype_has.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

    if (objOList_relationtype_is_of_type.Any())
    {
        OItem_relationtype_is_of_type = new clsOntologyItem()
        {
            GUID = objOList_relationtype_is_of_type.First().ID_Other,
            Name = objOList_relationtype_is_of_type.First().Name_Other,
            GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_isinstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_isinstate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

    if (objOList_relationtype_isinstate.Any())
    {
        OItem_relationtype_isinstate = new clsOntologyItem()
        {
            GUID = objOList_relationtype_isinstate.First().ID_Other,
            Name = objOList_relationtype_isinstate.First().Name_Other,
            GUID_Parent = objOList_relationtype_isinstate.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

    if (objOList_relationtype_located_in.Any())
    {
        OItem_relationtype_located_in = new clsOntologyItem()
        {
            GUID = objOList_relationtype_located_in.First().ID_Other,
            Name = objOList_relationtype_located_in.First().Name_Other,
            GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_needs = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "relationtype_needs".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                       select objRef).ToList();

    if (objOList_relationtype_needs.Any())
    {
        OItem_relationtype_needs = new clsOntologyItem()
        {
            GUID = objOList_relationtype_needs.First().ID_Other,
            Name = objOList_relationtype_needs.First().Name_Other,
            GUID_Parent = objOList_relationtype_needs.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

    if (objOList_relationtype_offered_by.Any())
    {
        OItem_relationtype_offered_by = new clsOntologyItem()
        {
            GUID = objOList_relationtype_offered_by.First().ID_Other,
            Name = objOList_relationtype_offered_by.First().Name_Other,
            GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_sitz = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "relationtype_sitz".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                      select objRef).ToList();

    if (objOList_relationtype_sitz.Any())
    {
        OItem_relationtype_sitz = new clsOntologyItem()
        {
            GUID = objOList_relationtype_sitz.First().ID_Other,
            Name = objOList_relationtype_sitz.First().Name_Other,
            GUID_Parent = objOList_relationtype_sitz.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_relationtype_tel = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "relationtype_tel".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                     select objRef).ToList();

    if (objOList_relationtype_tel.Any())
    {
        OItem_relationtype_tel = new clsOntologyItem()
        {
            GUID = objOList_relationtype_tel.First().ID_Other,
            Name = objOList_relationtype_tel.First().Name_Other,
            GUID_Parent = objOList_relationtype_tel.First().ID_Parent_Other,
            Type = Globals.Type_RelationType
        };
    }
    else
    {
        throw new Exception("config err");
    }


}

private void get_Config_Objects()
{
    var objOList_object_partner_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_partner_management".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

    if (objOList_object_partner_management.Any())
    {
        OItem_object_partner_management = new clsOntologyItem()
        {
            GUID = objOList_object_partner_management.First().ID_Other,
            Name = objOList_object_partner_management.First().Name_Other,
            GUID_Parent = objOList_object_partner_management.First().ID_Parent_Other,
            Type = Globals.Type_Object
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_object_standard_addresszusatz = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "object_standard_addresszusatz".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

    if (objOList_object_standard_addresszusatz.Any())
    {
        OItem_object_standard_addresszusatz = new clsOntologyItem()
        {
            GUID = objOList_object_standard_addresszusatz.First().ID_Other,
            Name = objOList_object_standard_addresszusatz.First().Name_Other,
            GUID_Parent = objOList_object_standard_addresszusatz.First().ID_Parent_Other,
            Type = Globals.Type_Object
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_object_standard_emailtype = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_standard_emailtype".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

    if (objOList_object_standard_emailtype.Any())
    {
        OItem_object_standard_emailtype = new clsOntologyItem()
        {
            GUID = objOList_object_standard_emailtype.First().ID_Other,
            Name = objOList_object_standard_emailtype.First().Name_Other,
            GUID_Parent = objOList_object_standard_emailtype.First().ID_Parent_Other,
            Type = Globals.Type_Object
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_object_standard_teltype = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_standard_teltype".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

    if (objOList_object_standard_teltype.Any())
    {
        OItem_object_standard_teltype = new clsOntologyItem()
        {
            GUID = objOList_object_standard_teltype.First().ID_Other,
            Name = objOList_object_standard_teltype.First().Name_Other,
            GUID_Parent = objOList_object_standard_teltype.First().ID_Parent_Other,
            Type = Globals.Type_Object
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_object_standard_urltype = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_standard_urltype".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

    if (objOList_object_standard_urltype.Any())
    {
        OItem_object_standard_urltype = new clsOntologyItem()
        {
            GUID = objOList_object_standard_urltype.First().ID_Other,
            Name = objOList_object_standard_urltype.First().Name_Other,
            GUID_Parent = objOList_object_standard_urltype.First().ID_Parent_Other,
            Type = Globals.Type_Object
        };
    }
    else
    {
        throw new Exception("config err");
    }


}

private void get_Config_Classes()
{
    var objOList_adress_zusatz = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "adress-zusatz".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

    if (objOList_adress_zusatz.Any())
    {
                OItem_adress_zusatz = new clsOntologyItem()
        {
            GUID = objOList_adress_zusatz.First().ID_Other,
            Name = objOList_adress_zusatz.First().Name_Other,
            GUID_Parent = objOList_adress_zusatz.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_class_adress_zusatz = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "class_adress_zusatz".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

    if (objOList_class_adress_zusatz.Any())
    {
        OItem_class_adress_zusatz = new clsOntologyItem()
        {
            GUID = objOList_class_adress_zusatz.First().ID_Other,
            Name = objOList_class_adress_zusatz.First().Name_Other,
            GUID_Parent = objOList_class_adress_zusatz.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_class_email_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "class_email_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

    if (objOList_class_email_type.Any())
    {
        OItem_class_email_type = new clsOntologyItem()
        {
            GUID = objOList_class_email_type.First().ID_Other,
            Name = objOList_class_email_type.First().Name_Other,
            GUID_Parent = objOList_class_email_type.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_class_telefonnummern_typ = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_telefonnummern_typ".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

    if (objOList_class_telefonnummern_typ.Any())
    {
        OItem_class_telefonnummern_typ = new clsOntologyItem()
        {
            GUID = objOList_class_telefonnummern_typ.First().ID_Other,
            Name = objOList_class_telefonnummern_typ.First().Name_Other,
            GUID_Parent = objOList_class_telefonnummern_typ.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_class_url_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "class_url_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

    if (objOList_class_url_type.Any())
    {
        OItem_class_url_type = new clsOntologyItem()
        {
            GUID = objOList_class_url_type.First().ID_Other,
            Name = objOList_class_url_type.First().Name_Other,
            GUID_Parent = objOList_class_url_type.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_class_zusatz_typ = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "class_zusatz_typ".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

    if (objOList_class_zusatz_typ.Any())
    {
        OItem_class_zusatz_typ = new clsOntologyItem()
        {
            GUID = objOList_class_zusatz_typ.First().ID_Other,
            Name = objOList_class_zusatz_typ.First().Name_Other,
            GUID_Parent = objOList_class_zusatz_typ.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_address = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_address".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

    if (objOList_type_address.Any())
    {
        OItem_type_address = new clsOntologyItem()
        {
            GUID = objOList_type_address.First().ID_Other,
            Name = objOList_type_address.First().Name_Other,
            GUID_Parent = objOList_type_address.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_availabilities = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_availabilities".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

    if (objOList_type_availabilities.Any())
    {
        OItem_type_availabilities = new clsOntologyItem()
        {
            GUID = objOList_type_availabilities.First().ID_Other,
            Name = objOList_type_availabilities.First().Name_Other,
            GUID_Parent = objOList_type_availabilities.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_availability_data = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_availability_data".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

    if (objOList_type_availability_data.Any())
    {
        OItem_type_availability_data = new clsOntologyItem()
        {
            GUID = objOList_type_availability_data.First().ID_Other,
            Name = objOList_type_availability_data.First().Name_Other,
            GUID_Parent = objOList_type_availability_data.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_email_address = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_email_address".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

    if (objOList_type_email_address.Any())
    {
        OItem_type_email_address = new clsOntologyItem()
        {
            GUID = objOList_type_email_address.First().ID_Other,
            Name = objOList_type_email_address.First().Name_Other,
            GUID_Parent = objOList_type_email_address.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_etin = (from objOItem in objDBLevel_Config1.ObjectRels
                              where objOItem.ID_Object == cstrID_Ontology
                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                              where objRef.Name_Object.ToLower() == "type_etin".ToLower() && objRef.Ontology == Globals.Type_Class
                              select objRef).ToList();

    if (objOList_type_etin.Any())
    {
        OItem_type_etin = new clsOntologyItem()
        {
            GUID = objOList_type_etin.First().ID_Other,
            Name = objOList_type_etin.First().Name_Other,
            GUID_Parent = objOList_type_etin.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_familienstand = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_familienstand".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

    if (objOList_type_familienstand.Any())
    {
        OItem_type_familienstand = new clsOntologyItem()
        {
            GUID = objOList_type_familienstand.First().ID_Other,
            Name = objOList_type_familienstand.First().Name_Other,
            GUID_Parent = objOList_type_familienstand.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_geschlecht = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_geschlecht".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

    if (objOList_type_geschlecht.Any())
    {
        OItem_type_geschlecht = new clsOntologyItem()
        {
            GUID = objOList_type_geschlecht.First().ID_Other,
            Name = objOList_type_geschlecht.First().Name_Other,
            GUID_Parent = objOList_type_geschlecht.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_identifkationsnummer__idnr_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "type_identifkationsnummer__idnr_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                     select objRef).ToList();

    if (objOList_type_identifkationsnummer__idnr_.Any())
    {
        OItem_type_identifkationsnummer__idnr_ = new clsOntologyItem()
        {
            GUID = objOList_type_identifkationsnummer__idnr_.First().ID_Other,
            Name = objOList_type_identifkationsnummer__idnr_.First().Name_Other,
            GUID_Parent = objOList_type_identifkationsnummer__idnr_.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_images__graphic_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_images__graphic_".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

    if (objOList_type_images__graphic_.Any())
    {
        OItem_type_images__graphic_ = new clsOntologyItem()
        {
            GUID = objOList_type_images__graphic_.First().ID_Other,
            Name = objOList_type_images__graphic_.First().Name_Other,
            GUID_Parent = objOList_type_images__graphic_.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_kommunikationsangaben = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_kommunikationsangaben".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

    if (objOList_type_kommunikationsangaben.Any())
    {
        OItem_type_kommunikationsangaben = new clsOntologyItem()
        {
            GUID = objOList_type_kommunikationsangaben.First().ID_Other,
            Name = objOList_type_kommunikationsangaben.First().Name_Other,
            GUID_Parent = objOList_type_kommunikationsangaben.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_land = (from objOItem in objDBLevel_Config1.ObjectRels
                              where objOItem.ID_Object == cstrID_Ontology
                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                              where objRef.Name_Object.ToLower() == "type_land".ToLower() && objRef.Ontology == Globals.Type_Class
                              select objRef).ToList();

    if (objOList_type_land.Any())
    {
        OItem_type_land = new clsOntologyItem()
        {
            GUID = objOList_type_land.First().ID_Other,
            Name = objOList_type_land.First().Name_Other,
            GUID_Parent = objOList_type_land.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                where objOItem.ID_Object == cstrID_Ontology
                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                select objRef).ToList();

    if (objOList_type_module.Any())
    {
        OItem_type_module = new clsOntologyItem()
        {
            GUID = objOList_type_module.First().ID_Other,
            Name = objOList_type_module.First().Name_Other,
            GUID_Parent = objOList_type_module.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_nat_rliche_person = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_nat_rliche_person".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

    if (objOList_type_nat_rliche_person.Any())
    {
        OItem_type_nat_rliche_person = new clsOntologyItem()
        {
            GUID = objOList_type_nat_rliche_person.First().ID_Other,
            Name = objOList_type_nat_rliche_person.First().Name_Other,
            GUID_Parent = objOList_type_nat_rliche_person.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_ort = (from objOItem in objDBLevel_Config1.ObjectRels
                             where objOItem.ID_Object == cstrID_Ontology
                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                             where objRef.Name_Object.ToLower() == "type_ort".ToLower() && objRef.Ontology == Globals.Type_Class
                             select objRef).ToList();

    if (objOList_type_ort.Any())
    {
        OItem_type_ort = new clsOntologyItem()
        {
            GUID = objOList_type_ort.First().ID_Other,
            Name = objOList_type_ort.First().Name_Other,
            GUID_Parent = objOList_type_ort.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_partner = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_partner".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

    if (objOList_type_partner.Any())
    {
        OItem_type_partner = new clsOntologyItem()
        {
            GUID = objOList_type_partner.First().ID_Other,
            Name = objOList_type_partner.First().Name_Other,
            GUID_Parent = objOList_type_partner.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_partner_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_partner_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

    if (objOList_type_partner_module.Any())
    {
        OItem_type_partner_module = new clsOntologyItem()
        {
            GUID = objOList_type_partner_module.First().ID_Other,
            Name = objOList_type_partner_module.First().Name_Other,
            GUID_Parent = objOList_type_partner_module.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_postleitzahl = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_postleitzahl".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

    if (objOList_type_postleitzahl.Any())
    {
        OItem_type_postleitzahl = new clsOntologyItem()
        {
            GUID = objOList_type_postleitzahl.First().ID_Other,
            Name = objOList_type_postleitzahl.First().Name_Other,
            GUID_Parent = objOList_type_postleitzahl.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_sozialversicherungsnummer = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_sozialversicherungsnummer".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

    if (objOList_type_sozialversicherungsnummer.Any())
    {
        OItem_type_sozialversicherungsnummer = new clsOntologyItem()
        {
            GUID = objOList_type_sozialversicherungsnummer.First().ID_Other,
            Name = objOList_type_sozialversicherungsnummer.First().Name_Other,
            GUID_Parent = objOList_type_sozialversicherungsnummer.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_steuernummer = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_steuernummer".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

    if (objOList_type_steuernummer.Any())
    {
        OItem_type_steuernummer = new clsOntologyItem()
        {
            GUID = objOList_type_steuernummer.First().ID_Other,
            Name = objOList_type_steuernummer.First().Name_Other,
            GUID_Parent = objOList_type_steuernummer.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_telefonnummer = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_telefonnummer".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

    if (objOList_type_telefonnummer.Any())
    {
        OItem_type_telefonnummer = new clsOntologyItem()
        {
            GUID = objOList_type_telefonnummer.First().ID_Other,
            Name = objOList_type_telefonnummer.First().Name_Other,
            GUID_Parent = objOList_type_telefonnummer.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_url = (from objOItem in objDBLevel_Config1.ObjectRels
                             where objOItem.ID_Object == cstrID_Ontology
                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                             where objRef.Name_Object.ToLower() == "type_url".ToLower() && objRef.Ontology == Globals.Type_Class
                             select objRef).ToList();

    if (objOList_type_url.Any())
    {
        OItem_type_url = new clsOntologyItem()
        {
            GUID = objOList_type_url.First().ID_Other,
            Name = objOList_type_url.First().Name_Other,
            GUID_Parent = objOList_type_url.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_web_service = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_web_service".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

    if (objOList_type_web_service.Any())
    {
        OItem_type_web_service = new clsOntologyItem()
        {
            GUID = objOList_type_web_service.First().ID_Other,
            Name = objOList_type_web_service.First().Name_Other,
            GUID_Parent = objOList_type_web_service.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }

    var objOList_type_weekdays = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_weekdays".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

    if (objOList_type_weekdays.Any())
    {
        OItem_type_weekdays = new clsOntologyItem()
        {
            GUID = objOList_type_weekdays.First().ID_Other,
            Name = objOList_type_weekdays.First().Name_Other,
            GUID_Parent = objOList_type_weekdays.First().ID_Parent_Other,
            Type = Globals.Type_Class
        };
    }
    else
    {
        throw new Exception("config err");
    }


}
        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}