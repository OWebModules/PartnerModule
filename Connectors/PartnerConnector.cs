﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using PartnerModule.Factories;
using PartnerModule.Models;
using PartnerModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Connectors
{
    public enum ComItemType
    {
        Email,
        Fax,
        Tel,
        Url
    }

    public class PartnerConnector : AppController
    {

        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
        }
        private ServiceAgentElastic serviceAgentElastic;
        private PartnerFactory partnerFactory;

        public async Task<List<Address>> GetAddressFull(List<clsOntologyItem> addressesRaw)
        {
            var serviceResultTask = await serviceAgentElastic.GetAddresses(addressesRaw);

            var serviceResult = serviceResultTask;

            var addresses = (from address in addressesRaw
                             join strasse in serviceResult.Strassen on address.GUID equals strasse.ID_Object into strassen
                             from strasse in strassen.DefaultIfEmpty()
                             join postfach in serviceResult.Postfaecher on address.GUID equals postfach.ID_Object into postfaecher
                             from postfach in postfaecher.DefaultIfEmpty()
                             join plzItem in serviceResult.Plzs on address.GUID equals plzItem.ID_Object
                             join ortItem in serviceResult.Orte on address.GUID equals ortItem.ID_Object
                             join land in serviceResult.Lands on ortItem.ID_Other equals land.ID_Object
                             select new Address
                             {
                                 Id = address.GUID,
                                 Name = address.Name,
                                 IdStrasse = strasse != null ? strasse.ID_Attribute : null,
                                 NameStrasse = strasse != null ? strasse.Val_String : null,
                                 IdPostfach = postfach != null ? postfach.ID_Attribute : null,
                                 NamePostfach = postfach != null ? postfach.Val_String : null,
                                 IdPlz = plzItem.ID_Other,
                                 Plz = plzItem.Name_Other,
                                 IdOrt = ortItem.ID_Other,
                                 NameOrt = ortItem.Name_Other,
                                 IdLand = land.ID_Other,
                                 NameLand = land.Name_Other
                             }).ToList();

            return addresses;
        }

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string id, string type)
        {
            return await serviceAgentElastic.GetOItem(id, type);
        }

        public ResultItem<List<ComGridItem>> GetEmptyComItemList()
        {
            return partnerFactory.CreateEmptyComItemList(LocalConfig);
        }

        public ResultItem<List<clsOntologyItem>> GetGenderList()
        {
            var elasticAgent = new ServiceAgentElastic(localConfig);
            return elasticAgent.GetGenderList();
        }

        public ResultItem<List<clsOntologyItem>> GetFamilyStatusList()
        {
            var elasticAgent = new ServiceAgentElastic(localConfig);
            return elasticAgent.GetFamiliyStatusList();
        }

        public async Task<ResultItem<List<ComGridItem>>> GetComItemList(Partner partner)
        {
            var factoryResult = await partnerFactory.CreateComItemList(partner,
                localConfig);

            return factoryResult;
        }

        public async Task<ResultItem<Partner>> GetPartnerData(clsOntologyItem partnerItem)
        {
            var result = new ResultItem<Partner>
            {
                ResultState = localConfig.Globals.LState_Success.Clone(),
                Result = new Partner()
            };

            var resultPartnerData = await serviceAgentElastic.GetPartnersByRef(partnerItem);

            if (resultPartnerData.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultPartnerData.Result;
                return result;
            }

            var resultFactory = await partnerFactory.CreatePartner(resultPartnerData, localConfig);

            if (resultFactory.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultFactory.ResultState;
            }

            result.Result = resultFactory.Result.FirstOrDefault();
            return result;
        }

        public async Task<ResultItem<List<Partner>>> GetPartnerList(clsOntologyItem refItem)
        {
            var result = new ResultItem<List<Partner>>
            {
                ResultState = localConfig.Globals.LState_Success.Clone(),
                Result = new List<Partner>()
            };

            var resultPartnerData = await serviceAgentElastic.GetPartnersByRef(refItem);

            if (resultPartnerData.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultPartnerData.Result;
                return result;
            }

            var resultFactory = await partnerFactory.CreatePartner(resultPartnerData, localConfig);

            if (resultFactory.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultFactory.ResultState;
            }

            result.Result = resultFactory.Result;
            return result;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveVorname(Partner partner, string vorname)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveNatuerlichePerson(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.PersonalData == null)
            {
                partner.PersonalData = new PersonalData
                {
                    Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                    Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                };
            }
             

            result = await serviceAgentElastic.SaveVorname(new clsOntologyItem
            {
                GUID = partner.PersonalData.Id,
                Name = partner.PersonalData.Name,
                GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                Type = localConfig.Globals.Type_Object
            }, vorname);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.PersonalData.IdVorname = result.Result.ID_Attribute;
            partner.PersonalData.NameVorname = result.Result.Val_String;
            
            return result;

        }

        public async Task<ResultItem<clsObjectRel>> SaveGeschlecht(Partner partner, string idGeschlecht)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectRel>>(async() =>
            {
                var result = new ResultItem<clsObjectRel>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone()
                };
                

                var elasticAgent = new ServiceAgentElastic(localConfig);
                if (partner.PersonalData == null)
                {
                    partner.PersonalData = new PersonalData
                    {
                        Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                        Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                    };
                }


                var geschlecht = await elasticAgent.GetOItem(idGeschlecht, localConfig.Globals.Type_Object);
                if (geschlecht.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = geschlecht.ResultState;
                    result.ResultState.Additional1 = "Error while getting the Gender";
                    return result;
                }

                var resultSave = await elasticAgent.SaveGeschlecht(new clsOntologyItem
                {
                    GUID = partner.PersonalData.Id,
                    Name = partner.PersonalData.Name,
                    GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                    Type = localConfig.Globals.Type_Object
                }, geschlecht.Result);

                if (resultSave.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = resultSave.ResultState;
                    return result;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectRel>> SaveFamilienstand(Partner partner, string idFamilienstand)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectRel>>(async () =>
            {
                var result = new ResultItem<clsObjectRel>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone()
                };


                var elasticAgent = new ServiceAgentElastic(localConfig);
                if (partner.PersonalData == null)
                {
                    partner.PersonalData = new PersonalData
                    {
                        Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                        Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                    };
                }


                var familienStand = await elasticAgent.GetOItem(idFamilienstand, localConfig.Globals.Type_Object);
                if (familienStand.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = familienStand.ResultState;
                    result.ResultState.Additional1 = "Error while getting the Gender";
                    return result;
                }

                var resultSave = await elasticAgent.SaveFamilienstand(new clsOntologyItem
                {
                    GUID = partner.PersonalData.Id,
                    Name = partner.PersonalData.Name,
                    GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                    Type = localConfig.Globals.Type_Object
                }, familienStand.Result);

                if (resultSave.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = resultSave.ResultState;
                    return result;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveName(Partner partner, string name)
        {

            var result = localConfig.Globals.LState_Success.Clone();

            result = await serviceAgentElastic.SaveName(partner.OItemPartner, name);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                partner.PersonalData.Name = name;
            }
            
            return result;
        }

        public async Task<ResultItem<PlzOrtLand>> SavePLZOrt(Partner partner, clsOntologyItem ortOrPLZ)
        {

            var result = new ResultItem<PlzOrtLand>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveAddress(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.Address == null)
            {
                partner.Address = new Address
                {
                    Id = serviceAgentElastic.OItemAddress.GUID,
                    Name = serviceAgentElastic.OItemAddress.Name
                };
            }


            result.ResultState = await serviceAgentElastic.SavePLZOrOrt(new clsOntologyItem
            {
                GUID = partner.Address.Id,
                Name = partner.Address.Name,
                GUID_Parent = localConfig.OItem_type_address.GUID,
                Type = localConfig.Globals.Type_Object
            }, ortOrPLZ);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (ortOrPLZ.GUID_Parent == localConfig.OItem_type_ort.GUID)
            {
                partner.Address.IdOrt = ortOrPLZ.GUID;
                partner.Address.NameOrt = ortOrPLZ.Name;
                if (serviceAgentElastic.OItemPlz != null)
                {
                    partner.Address.IdPlz = serviceAgentElastic.OItemPlz.GUID;
                    partner.Address.Plz = serviceAgentElastic.OItemPlz.Name;
                }

                if (serviceAgentElastic.OItemLand != null)
                {
                    partner.Address.IdLand = serviceAgentElastic.OItemLand.GUID;
                    partner.Address.NameLand = serviceAgentElastic.OItemLand.Name;
                }

                result.Result = new PlzOrtLand
                {
                    OItemOrt = ortOrPLZ,
                    OItemPlz = serviceAgentElastic.OItemPlz,
                    OItemLand = serviceAgentElastic.OItemLand
                };
            }
            else
            {
                partner.Address.IdPlz = ortOrPLZ.GUID;
                partner.Address.Plz = ortOrPLZ.Name;
                if (serviceAgentElastic.OItemOrt != null)
                {
                    partner.Address.IdPlz = serviceAgentElastic.OItemOrt.GUID;
                    partner.Address.Plz = serviceAgentElastic.OItemOrt.Name;
                }

                if (serviceAgentElastic.OItemLand != null)
                {
                    partner.Address.IdLand = serviceAgentElastic.OItemLand.GUID;
                    partner.Address.NameLand = serviceAgentElastic.OItemLand.Name;
                }

                result.Result = new PlzOrtLand
                {
                    OItemPlz = ortOrPLZ,
                    OItemOrt = serviceAgentElastic.OItemOrt,
                    OItemLand = serviceAgentElastic.OItemLand
                };
            }

            return result;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveStrasse(Partner partner, string strasse)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveAddress(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.Address!= null)
            {
                partner.Address = new Address
                {
                    Id = serviceAgentElastic.OItemAddress.GUID,
                    Name = serviceAgentElastic.OItemAddress.Name
                };
            }


            result = await serviceAgentElastic.SaveStrasse(new clsOntologyItem
            {
                GUID = partner.Address.Id,
                Name = partner.Address.Name,
                GUID_Parent = localConfig.OItem_type_address.GUID,
                Type = localConfig.Globals.Type_Object
            }, strasse);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.Address.IdStrasse = result.Result.ID_Attribute;
            partner.Address.NameStrasse = result.Result.Val_String;

            return result;

        }

        public async Task<ResultItem<clsObjectAtt>> SavePostfach(Partner partner, string postfach)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveAddress(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.Address != null)
            {
                partner.Address = new Address
                {
                    Id = serviceAgentElastic.OItemAddress.GUID,
                    Name = serviceAgentElastic.OItemAddress.Name
                };
            }


            result = await serviceAgentElastic.SavePostfach(new clsOntologyItem
            {
                GUID = partner.Address.Id,
                Name = partner.Address.Name,
                GUID_Parent = localConfig.OItem_type_address.GUID,
                Type = localConfig.Globals.Type_Object
            }, postfach);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.Address.IdPostfach = result.Result.ID_Attribute;
            partner.Address.NamePostfach = result.Result.Val_String;

            return result;

        }

        public async Task<ResultItem<clsObjectAtt>> SaveNachname(Partner partner, string nachname)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveNatuerlichePerson(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.PersonalData == null)
            {
                partner.PersonalData = new PersonalData
                {
                    Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                    Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                };
            }


            result = await serviceAgentElastic.SaveNachname(new clsOntologyItem
            {
                GUID = partner.PersonalData.Id,
                Name = partner.PersonalData.Name,
                GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                Type = localConfig.Globals.Type_Object
            }, nachname);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.PersonalData.IdNachname = result.Result.ID_Attribute;
            partner.PersonalData.NameNachname = result.Result.Val_String;

            return result;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveGeburtsdatum(Partner partner, DateTime geburtsdatum)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveNatuerlichePerson(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.PersonalData == null)
            {
                partner.PersonalData = new PersonalData
                {
                    Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                    Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                };
            }


            result = await serviceAgentElastic.SaveGeburtsdatum(new clsOntologyItem
            {
                GUID = partner.PersonalData.Id,
                Name = partner.PersonalData.Name,
                GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                Type = localConfig.Globals.Type_Object
            }, geburtsdatum);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.PersonalData.IdGeburtsdatum = result.Result.ID_Attribute;
            partner.PersonalData.Geburtsdatum = result.Result.Val_Date;

            return result;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveTodesdatum(Partner partner, DateTime todesdatum)
        {

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            result.ResultState = await serviceAgentElastic.SaveNatuerlichePerson(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.PersonalData == null)
            {
                partner.PersonalData = new PersonalData
                {
                    Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                    Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                };
            }


            result = await serviceAgentElastic.SaveTodesdatum(new clsOntologyItem
            {
                GUID = partner.PersonalData.Id,
                Name = partner.PersonalData.Name,
                GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                Type = localConfig.Globals.Type_Object
            }, todesdatum);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            partner.PersonalData.IdTodesdatum = result.Result.ID_Attribute;
            partner.PersonalData.Todesdatum = result.Result.Val_Date;

            return result;
        }

        public async Task<clsOntologyItem> SavePartner(Partner partner)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = localConfig.Globals.LState_Success.Clone();
                var elasticAgent = new ServiceAgentElastic(localConfig);

                if (partner.OItemPartner == null)
                {
                    partner.OItemPartner = new clsOntologyItem
                    {
                        GUID = localConfig.Globals.NewGUID,
                        Name = partner.Name,
                        GUID_Parent = localConfig.OItem_type_partner.GUID,
                        Type = localConfig.Globals.Type_Object
                    };
                }

                if (string.IsNullOrEmpty(partner.OItemPartner.GUID))
                {
                    partner.OItemPartner.GUID = localConfig.Globals.NewGUID;
                }
                
                    
                var serviceResult = await elasticAgent.SavePartner(partner.OItemPartner);

                if (serviceResult.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    serviceResult.Additional1 = "Error while saving Partner-Item";
                    return result;
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SavePersonalData(clsOntologyItem personalItem, Partner partner)
        {

            var result = await serviceAgentElastic.SaveNatuerlichePerson(partner.OItemPartner);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.PersonalData == null)
            {
                partner.PersonalData = new PersonalData
                {
                    Id = serviceAgentElastic.OItemNatuerlichePerson.GUID,
                    Name = serviceAgentElastic.OItemNatuerlichePerson.Name
                };
            }

            result = await serviceAgentElastic.SaveNatuerlichPersonData(serviceAgentElastic.OItemNatuerlichePerson, personalItem);

            return result;
        }

        public async Task<ResultItem<List<ComGridItem>>> SaveEmailAddress(clsOntologyItem communicationItem, Partner partner)
        {
            return await SaveCommunicationItem(communicationItem, partner, localConfig.OItem_relationtype_contains, ComItemType.Email);
        }

        public async Task<ResultItem<List<ComGridItem>>> SaveTelNumber(clsOntologyItem communicationItem, Partner partner)
        {
            return await SaveCommunicationItem(communicationItem, partner, localConfig.OItem_relationtype_tel, ComItemType.Tel);
        }

        public async Task<ResultItem<List<ComGridItem>>> SaveFaxNumber(clsOntologyItem communicationItem, Partner partner)
        {
            return await SaveCommunicationItem(communicationItem, partner, localConfig.OItem_relationtype_fax, ComItemType.Fax);
        }

        public async Task<ResultItem<List<ComGridItem>>> SaveUrlNumber(clsOntologyItem communicationItem, Partner partner)
        {
            return await SaveCommunicationItem(communicationItem, partner, localConfig.OItem_relationtype_contains, ComItemType.Url);
        }

        public async Task<ResultItem<List<ComGridItem>>> SaveCommunicationItem(clsOntologyItem communicationItem, Partner partner, clsOntologyItem relationTypeItem, ComItemType comItemType)
        {

            var result = new ResultItem<List<ComGridItem>>
            {
                ResultState = localConfig.Globals.LState_Success.Clone(),
                Result = new List<ComGridItem>()
            };

            result.ResultState = await serviceAgentElastic.SaveKommunikationsangaben(partner.OItemPartner);

            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (partner.CommunicationData == null)
            {
                partner.CommunicationData = new CommunicationData
                {
                    Id = serviceAgentElastic.OItemKommunikationsangaben.GUID,
                    Name = serviceAgentElastic.OItemKommunikationsangaben.Name
                };
            }

            
            result.ResultState = await serviceAgentElastic.SaveKommunikationsAngabenData(new clsOntologyItem
                {
                    GUID = partner.CommunicationData.Id,
                    Name = partner.CommunicationData.Name,
                    GUID_Parent = localConfig.OItem_type_kommunikationsangaben.GUID,
                    Type = localConfig.Globals.Type_Object
                }, communicationItem
                , relationTypeItem);


            if (result.ResultState.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (partner.CommunicationData.EmailAddress == null)
                {
                    partner.CommunicationData.EmailAddress = new List<EmailAddress>();
                }

                var emailAddressItem = new EmailAddress
                {
                    Id = communicationItem.GUID,
                    Name = communicationItem.Name
                };
                partner.CommunicationData.EmailAddress.Add(emailAddressItem);

                var comItemTypeStr = "Email";

                if (comItemType == ComItemType.Tel)
                {
                    comItemTypeStr = "Tel";
                }
                else if (comItemType == ComItemType.Fax)
                {
                    comItemTypeStr = "Fax";
                } else if  (comItemType == ComItemType.Url)
                {
                    comItemTypeStr = "Url";
                }

                result.Result.Add(new ComGridItem
                {
                    NameType = comItemTypeStr,
                    IdComItem = emailAddressItem.Id,
                    NameComItem = emailAddressItem.Name
                });
            }
            return result;
        }

        public PartnerConnector(Globals globals) : base(globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            partnerFactory = new PartnerFactory();
        }
    }

    public class PlzOrtLand
    {
        public clsOntologyItem OItemPlz { get; set; }
        public clsOntologyItem OItemOrt { get; set; }
        public clsOntologyItem OItemLand { get; set; }
    }
}
