﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class PersonalData
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string IdVorname { get; set; }
        public string NameVorname { get; set; }
        public string IdNachname { get; set; }
        public string NameNachname { get; set; }

        public string IdGeburtsdatum { get; set; }
        public DateTime? Geburtsdatum { get; set; }

        public string IdGeburtsort { get; set; }
        public string NameGeburtsort { get; set; }

        public string IdTodesdatum { get; set; }
        public DateTime? Todesdatum { get; set; }

        public string IdNamenstag { get; set; }
        public DateTime? Namenstag { get; set; }

        public string IdAnrede { get; set; }
        public string NameAnrede { get; set; }

        public string IdETin { get; set; }
        public string ETin { get; set; }

        public string IdFamilienstand { get; set; }
        public string NameFamilienstand { get; set; }

        public string IdGeschlecht { get; set; }
        public string NameGeschlecht { get; set; }

        public string IdIdentifikationsNummer { get; set; }
        public string NameIdentifikationsNummer { get; set; }

        public string IdSozialversicherungsNummer { get; set; }
        public string NameSozialversicherungsNummer { get; set; }

        public string IdSteuerNummer { get; set; }
        public string NameSteuerNummer { get; set; }
    }
}
