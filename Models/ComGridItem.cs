﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ComGridItem
    {
        [KendoColumn(hidden = true)]
        public string IdType { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Typ")]
        public string NameType { get; set; }

        [KendoColumn(hidden = true)]
        public string IdComItem { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Adresse")]
        public string NameComItem { get; set; }
    }
}
