﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class CommunicationData
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public List<PhoneNumber> PhoneNumbers { get; set; }
        public List<PhoneNumber> FaxNumbers { get; set; }

        public List<EmailAddress> EmailAddress { get; set; }
        public List<Url> Urls { get; set; }
    }
}
