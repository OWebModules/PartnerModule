﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class Partner
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Address Address { get; set; }
        public PersonalData PersonalData { get; set; }
        public CommunicationData CommunicationData { get; set; }

        public clsOntologyItem OItemPartner { get; set; }
    }
}
