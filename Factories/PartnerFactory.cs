﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PartnerModule.Models;
using PartnerModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Factories
{
    public class PartnerFactory
    {
        public ResultItem<List<ComGridItem>> CreateEmptyComItemList(clsLocalConfig localConfig)
        {
            var result = new ResultItem<List<ComGridItem>>
            {
                ResultState = localConfig.Globals.LState_Success.Clone(),
                Result = new List<ComGridItem>()
            };

            return result;
        }

        public async Task<ResultItem<List<ComGridItem>>> CreateComItemList(Partner partner,
            clsLocalConfig localConfig)
        {
            var taskResult = await Task.Run<ResultItem<List<ComGridItem>>>(() =>
            {
                var result = new ResultItem<List<ComGridItem>>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new List<ComGridItem>()
                };


                if (partner.CommunicationData != null)
                {
                    result.Result.AddRange(partner.CommunicationData.PhoneNumbers.Select(phoneNumber => new ComGridItem
                    {
                        NameType = "Tel",
                        IdComItem = phoneNumber.Id,
                        NameComItem = phoneNumber.Name
                    }));

                    result.Result.AddRange(partner.CommunicationData.FaxNumbers.Select(phoneNumber => new ComGridItem
                    {
                        NameType = "Fax",
                        IdComItem = phoneNumber.Id,
                        NameComItem = phoneNumber.Name
                    }));

                    result.Result.AddRange(partner.CommunicationData.EmailAddress.Select(phoneNumber => new ComGridItem
                    {
                        NameType = "Email",
                        IdComItem = phoneNumber.Id,
                        NameComItem = phoneNumber.Name
                    }));

                    result.Result.AddRange(partner.CommunicationData.Urls.Select(phoneNumber => new ComGridItem
                    {
                        NameType = "Url",
                        IdComItem = phoneNumber.IdUrl,
                        NameComItem = phoneNumber.NameUrl
                    }));
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ComGridItem>> GetNewComItem(clsObjectRel communicationToItem, clsLocalConfig localConfig)
        {
            var taskResult = await Task.Run<ResultItem<ComGridItem>>(() =>
            {
                var result = new ResultItem<ComGridItem>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new ComGridItem()
                };

                if (communicationToItem.ID_RelationType == localConfig.OItem_relationtype_tel.GUID)
                {
                    result.Result.NameType = "Tel";
                }
                else if (communicationToItem.ID_RelationType == localConfig.OItem_relationtype_fax.GUID)
                {
                    result.Result.NameType = "Fax";
                }
                else if (communicationToItem.ID_Parent_Other == localConfig.OItem_type_email_address.GUID)
                {
                    result.Result.NameType = "Email";
                }
                else if (communicationToItem.ID_Parent_Other == localConfig.OItem_type_url.GUID)
                {
                    result.Result.NameType = "Url";
                }

                result.Result.IdComItem = communicationToItem.ID_Other;
                result.Result.NameComItem = communicationToItem.Name_Other;

                return result;
            });



            return taskResult;
        }

        public async Task<ResultItem<List<Partner>>> CreatePartner(ServiceResult partnerData, clsLocalConfig localConfig)
        {
            var taskResult = await Task.Run<ResultItem<List<Partner>>>(() =>
            {
                var result = new ResultItem<List<Partner>>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new List<Partner>()
                };

                result.Result = partnerData.PartnerList.Select(partnerItm => new Partner
                {
                    Id = partnerItm.GUID,
                    Name = partnerItm.Name,
                    OItemPartner = partnerItm
                }).ToList();

                result.Result.ForEach(partner =>
                {
                    partner.PersonalData = (from personalDataToPartner in partnerData.PersonalDataToPartner.Where(partPers => partPers.ID_Other == partner.Id)
                                            join vorname in partnerData.PersonalDataToVorname on personalDataToPartner.ID_Object equals vorname.ID_Object into vornamen
                                            from vorname in vornamen.DefaultIfEmpty()
                                            join nachname in partnerData.PersonalDataToNachname on personalDataToPartner.ID_Object equals nachname.ID_Object into nachnamen
                                            from nachname in nachnamen.DefaultIfEmpty()
                                            join geburtsdatum in partnerData.PersonalDataToGeburtsdatum on personalDataToPartner.ID_Object equals geburtsdatum.ID_Object into geburtsdaten
                                            from geburtsdatum in geburtsdaten.DefaultIfEmpty()
                                            join todesdatum in partnerData.PersonalDataToTodesdatum on personalDataToPartner.ID_Object equals todesdatum.ID_Object into todesdatums
                                            from todesdatum in todesdatums.DefaultIfEmpty()
                                            join etin in partnerData.PersonalDataToETin on personalDataToPartner.ID_Object equals etin.ID_Object into etins
                                            from etin in etins.DefaultIfEmpty()
                                            join familienstand in partnerData.PersonalDataToFamilienstand on personalDataToPartner.ID_Object equals familienstand.ID_Object into familienstands
                                            from familienstand in familienstands.DefaultIfEmpty()
                                            join geburtsort in partnerData.PersonalDataToGeburtsort on personalDataToPartner.ID_Object equals geburtsort.ID_Object into geburtsorte
                                            from geburtsort in geburtsorte.DefaultIfEmpty()
                                            join geschlecht in partnerData.PersonalDataToGeschlecht on personalDataToPartner.ID_Object equals geschlecht.ID_Object into geschlechter
                                            from geschlecht in geschlechter.DefaultIfEmpty()
                                            join identifikationsnummer in partnerData.PersonalDataToIdentifikationsnummer on personalDataToPartner.ID_Object equals identifikationsnummer.ID_Object into identifikationsnummern
                                            from identifikationsnummer in identifikationsnummern.DefaultIfEmpty()
                                            join sozialversicherungsnummer in partnerData.PersonalDataToSozialversicherungsnummer on personalDataToPartner.ID_Object equals sozialversicherungsnummer.ID_Object into sozialversicherungsnummern
                                            from sozialversicherungsnummer in sozialversicherungsnummern.DefaultIfEmpty()
                                            join steuernummer in partnerData.PersonalDataToSteuernummer on personalDataToPartner.ID_Object equals steuernummer.ID_Object into steuernummern
                                            from steuernummer in steuernummern.DefaultIfEmpty()
                                            select new PersonalData
                                            {
                                                Id = personalDataToPartner.ID_Object,
                                                Name = personalDataToPartner.Name_Object,
                                                IdVorname = vorname?.ID_Attribute,
                                                NameVorname = vorname?.Val_String,
                                                IdNachname = nachname?.ID_Attribute,
                                                NameNachname = nachname?.Val_String,
                                                IdGeburtsdatum = geburtsdatum?.ID_Attribute,
                                                Geburtsdatum = geburtsdatum?.Val_Datetime,
                                                IdTodesdatum = todesdatum?.ID_Attribute,
                                                Todesdatum = todesdatum?.Val_Datetime,
                                                IdETin = etin?.ID_Other,
                                                ETin = etin?.Name_Other,
                                                IdFamilienstand = familienstand?.ID_Other,
                                                NameFamilienstand = familienstand?.Name_Other,
                                                IdGeburtsort = geburtsort?.ID_Other,
                                                NameGeburtsort = geburtsort?.Name_Other,
                                                IdGeschlecht = geschlecht?.ID_Other,
                                                NameGeschlecht = geschlecht?.Name_Other,
                                                IdIdentifikationsNummer = identifikationsnummer?.ID_Other,
                                                NameIdentifikationsNummer = identifikationsnummer?.Name_Other,
                                                IdSozialversicherungsNummer = sozialversicherungsnummer?.ID_Other,
                                                NameSozialversicherungsNummer = sozialversicherungsnummer?.Name_Other,
                                                IdSteuerNummer = steuernummer?.ID_Other,
                                                NameSteuerNummer = steuernummer?.Name_Other
                                                
                                            }).FirstOrDefault();

                    partner.Address = (from partnerToAddress in partnerData.PartnersToAddresses.Where(partAddr => partAddr.ID_Object == partner.Id)
                                       join strasse in partnerData.Strassen on partnerToAddress.ID_Other equals strasse.ID_Object into strassen
                                       from strasse in strassen.DefaultIfEmpty()
                                       join postfach in partnerData.Postfaecher on partnerToAddress.ID_Other equals postfach.ID_Object into postfaecher
                                       from postfach in postfaecher.DefaultIfEmpty()
                                       join plzItem in partnerData.Plzs on partnerToAddress.ID_Other equals plzItem.ID_Object
                                       join ortItem in partnerData.Orte on partnerToAddress.ID_Other equals ortItem.ID_Object
                                       join land in partnerData.Lands on ortItem.ID_Other equals land.ID_Object
                                       select new Address
                                       {
                                           Id = partnerToAddress.ID_Other,
                                           Name = partnerToAddress.Name_Other,
                                           IdStrasse = strasse != null ? strasse.ID_Attribute : null,
                                           NameStrasse = strasse != null ? strasse.Val_String : null,
                                           IdPostfach = postfach != null ? postfach.ID_Attribute : null,
                                           NamePostfach = postfach != null ? postfach.Val_String : null,
                                           IdPlz = plzItem.ID_Other,
                                           Plz = plzItem.Name_Other,
                                           IdOrt = ortItem.ID_Other,
                                           NameOrt = ortItem.Name_Other,
                                           IdLand = land.ID_Other,
                                           NameLand = land.Name_Other
                                       }).FirstOrDefault();
                    partner.CommunicationData = (from communicationDat in partnerData.CommunicationToPartners.Where(partCom => partCom.ID_Other == partner.Id)
                                                 select new CommunicationData
                                                 {
                                                     Id = communicationDat.ID_Object,
                                                     Name = communicationDat.Name_Object
                                                 }).FirstOrDefault();

                    if (partner.CommunicationData != null)
                    {
                        partner.CommunicationData.EmailAddress = (from emailAddress in partnerData.CommunicationToEmailAddresses.Where(comItm => comItm.ID_Object == partner.CommunicationData.Id)
                                                                  select new EmailAddress
                                                                  {
                                                                      Id = emailAddress.ID_Other,
                                                                      Name = emailAddress.Name_Other
                                                                  }).ToList();

                        partner.CommunicationData.PhoneNumbers = (from phoneNumber in partnerData.CommunicationToPhoneNumbers.Where(comItm => comItm.ID_Object == partner.CommunicationData.Id)
                                                                  select new PhoneNumber
                                                                  {
                                                                      Id = phoneNumber.ID_Other,
                                                                      Name = phoneNumber.Name_Other
                                                                  }).ToList();

                        partner.CommunicationData.FaxNumbers = (from phoneNumber in partnerData.CommunicationToFaxs.Where(comItm => comItm.ID_Object == partner.CommunicationData.Id)
                                                                select new PhoneNumber
                                                                {
                                                                    Id = phoneNumber.ID_Other,
                                                                    Name = phoneNumber.Name_Other
                                                                }).ToList();

                        partner.CommunicationData.Urls = (from url in partnerData.CommunicationToUrls.Where(comItm => comItm.ID_Object == partner.CommunicationData.Id)
                                                          select new Url
                                                          {
                                                              IdUrl = url.ID_Other,
                                                              NameUrl = url.Name_Other
                                                          }).ToList();
                    }
                    
                });

                return result;
            });

            return taskResult;
            
        }
    }
}
